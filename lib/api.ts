import HooksRegister from "./HooksRegister";

const unauthenticatedHooksRegister = new HooksRegister();

export const config = {
  baseUrl: "http://localhost:3000/api/v1", // TODO: this should be probably null and we should throw when unset
  csrfToken: "", // set by UserSession
};

export function onUnauthenticated(hook: Function): Function {
  return unauthenticatedHooksRegister.add(hook);
}

export async function getUser() {
  return requestJson("/sessions/current");
}

export async function postTest() {
  return requestJson("/sessions/post_test", { method: "POST" });
}

/*
if (typeof window === "object") {
  window.getUser = getUser;
  window.postTest = postTest;
}
*/

export interface RequestJsonOptions {
  method: "GET" | "POST" | "DELETE" | "PUT" | "PATCH";
}

async function requestJson(
  path: string,
  options: RequestJsonOptions = {
    method: "GET",
  }
) {
  const url = `${config.baseUrl}${path}`;
  const headers = {
    "Content-Type": "application/json",
    "X-Csrf-Token": config.csrfToken,
  };

  const reqOptions: RequestInit = {
    method: options.method,
    mode: "cors",
    credentials: "include",
    headers,
  };
  const response = await fetch(url, reqOptions);

  if (response.status == 401) {
    unauthenticatedHooksRegister.trigger();
    throw new Error("Unauthenticated");
  }

  if (!response.ok) {
    throw new Error("Bad Response");
  }

  const json = await response.json();
  return json;
}
