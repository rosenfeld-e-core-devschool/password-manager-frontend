//import { Configuration, FrontendApi, Session, Identity } from "@ory/client"
import { edgeConfig } from "@ory/integrations/next"
import { getUser, config as apiConfig } from "./api";

import HooksRegister from "./HooksRegister";

//const ory = new FrontendApi(new Configuration(edgeConfig))
interface UserSessionProps{
    userId: string
    email: string
    logoutUrl: string
    csrfToken: string
}

class UserSession {
    session: UserSessionProps | undefined
    hooksRegister: HooksRegister
    logoutUrl: string
    loginUrl: string
    isLoading: boolean
    constructor() {
        this.isLoading = true
        this.hooksRegister = new HooksRegister;
        this.logoutUrl = `${apiConfig.baseUrl}/sessions/logout?csrf_token=`;
        this.loginUrl = edgeConfig.basePath + "/ui/login";
        getUser().then((session: UserSessionProps) => {
            this.session = session;
            apiConfig.csrfToken = session.csrfToken;
            this.logoutUrl = `${apiConfig.baseUrl}/sessions/logout?csrf_token=${session.csrfToken}`;
            this.hooksRegister.trigger();
            this.isLoading = false;
        }).catch(() => {
            this.isLoading = false;
        });
        /*
        ory.toSession()
            .then(({ data }: { data: any }) => {
                // User has a session!
                this.session = data;
                // Create a logout url
                ory.createBrowserLogoutFlow().then(({ data }: { data: any }) => {
                    this.logoutUrl = data.logout_url;
                    this.hooksRegister.trigger()
                    this.isLoading = false;
                })
            }).catch(() => {
                this.isLoading = false;
            })
        */
    }

    registerSessionChangeHook(hook: Function): Function { return this.hooksRegister.add(hook); }
}

export default new UserSession();