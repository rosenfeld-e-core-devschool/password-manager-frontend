export interface Hooks {
    [key: string]: Function
};

export default class HooksRegister {
    hooks: Hooks
    idx: number
    constructor() {
        this.hooks = {};
        this.idx = 0;
    }

    add(hook: Function): Function {
        const id = `h${++this.idx}`;
        this.hooks[id] = hook;
        return () => { delete this.hooks[id]; }
    }

    trigger() {
        Object.values(this.hooks).forEach(h => { h(); });
    }

    clear() { this.hooks = {}; }
}