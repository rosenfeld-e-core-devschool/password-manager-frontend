//import '@/styles/globals.css'
import type { AppProps } from 'next/app'

/*
export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}*/

import dynamic from "next/dynamic";
import Layout from "../components/layout";

function App({
  Component,
  pageProps,
}: AppProps) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  )
}

export default dynamic(() => Promise.resolve(App), {
  ssr: false,
});