import { Container, NextUIProvider } from '@nextui-org/react';
import Header from "./header";
import { ReactNode, useEffect, useState } from "react";
import userSession from "../lib/UserSession";
import AccessDenied from "./AccessDenied";
import LoadingPage from "./LoadingPage";

export default function Layout({ children }: { children: ReactNode }) {
    const [session, setSession] = useState<any>(userSession.session);
    useEffect(() => {
        const deregister = userSession.registerSessionChangeHook(() => { setSession(userSession.session) });
        return () => { deregister(); }; // remove TypeScript warning by ensuring return value is void
    }, [userSession]);

    const content = userSession.isLoading ? <LoadingPage /> :
        (session ? children : <AccessDenied />)
    return (
        <NextUIProvider>
            <Header session={session} />
            <Container>{content}</Container>
        </NextUIProvider>
    )
}