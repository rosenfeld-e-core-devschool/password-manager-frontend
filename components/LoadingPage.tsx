export default function LoadingPage() {
    return (
      <>
        <h1>Loading</h1>
        <p>Please wait...</p>
      </>
    )
  }