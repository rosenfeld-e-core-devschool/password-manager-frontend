import Link from "next/link"
import { Navbar, Text } from "@nextui-org/react";
import userSession from "../lib/UserSession";

export default function Header({ session } : { session: any }) {
    const email = session?.email;
    return (
        <Navbar isBordered variant="sticky">
            <Navbar.Brand>
                <Text>Password Manager</Text>
            </Navbar.Brand>
            <Navbar.Content>
                {!session && (
                    <Navbar.Link href={userSession.loginUrl}>Sign in</Navbar.Link>
                )}
                {email && (<>
                    <Navbar.Link href={userSession.logoutUrl}>Sign out</Navbar.Link>
                    <Text>Signed in as {email}</Text>
                </>

                )}

            </Navbar.Content>
        </Navbar>
    )
}